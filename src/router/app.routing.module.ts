import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './auth-guard.service';
import { LoginComponent } from './../app/login/login.component';


const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
]

@NgModule({
    imports: [
      CommonModule,
      RouterModule.forRoot(appRoutes)
    ],
    exports: [
      RouterModule
    ],
    declarations: [],
    providers: [AuthGuardService]
  })

  export class AppRoutingModule { }