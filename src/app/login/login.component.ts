import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public doughnutChartLabels:string[] = ['Good', 'Avg', 'Poor'];
  public doughnutChartData:number[] = [0, 68, 32];
  public doughnutChartType:string = 'doughnut';

  public chartColors: any[] = [
    { 
      backgroundColor:["#e84c3d", "#e9edf0", "#e84c3d"]
    }];
  public showAllData: boolean = true;
  public doughnutChartData1:number[] = [0, 42, 58];
  public chartColors1: any[] = [
      { 
        backgroundColor:["#f7941d", "#e9edf0", "#f7941d"]
      }];

  // public chartColors: Array<any> = [
  //   { // first color
  //     backgroundColor: 'rgba(225,10,24,0.2)',
  //     borderColor: 'rgba(225,10,24,0.2)',
  //     pointBackgroundColor: 'rgba(225,10,24,0.2)',
  //     pointBorderColor: '#fff',
  //     pointHoverBackgroundColor: '#fff',
  //     pointHoverBorderColor: 'rgba(225,10,24,0.2)'
  //   },
  //   { // second color
  //     backgroundColor: 'rgba(225,10,24,0.2)',
  //     borderColor: 'rgba(225,10,24,0.2)',
  //     pointBackgroundColor: 'rgba(225,10,24,0.2)',
  //     pointBorderColor: '#fff',
  //     pointHoverBackgroundColor: '#fff',
  //     pointHoverBorderColor: 'rgba(225,10,24,0.2)'
  //   },
  //   { // second color
  //     backgroundColor: 'rgba(225,10,24,0.2)',
  //     borderColor: 'rgba(225,10,24,0.2)',
  //     pointBackgroundColor: 'rgba(225,10,24,0.2)',
  //     pointBorderColor: '#fff',
  //     pointHoverBackgroundColor: '#fff',
  //     pointHoverBorderColor: 'rgba(225,10,24,0.2)'
  //   }];

  constructor() { }

  ngOnInit() {
  }

  giveWhiteBack(id) {
      let i = 1;
      for(let i=1; i<=6; i++) {
        if(id == i) {
          document.getElementById(Number(i) + "").classList.add("white");
        } else {
          document.getElementById(Number(i) + "").classList.remove("white");
      }
    }
    if(id == 3) {
      this.showAllData = true;
    } else {
      this.showAllData = false;
    }
  }
}
