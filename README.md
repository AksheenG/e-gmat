# e-gmat

<!-- Thank you for the opportunity. -->

<!-- This file contains information regarding the assignment provided by your firm.

1. The project is made using angularJS, HTML, SCSS.
2. I have used ng-charts library for charts required. Alternative for charts is provided in other tabs eg. Overall, Verbal RC, etc.
 Note: As per requirement, The data is shown in Verbal CR, which is the landing tab when app is loaded.
3. The required content is not fit for mobile view. Because header and navigation cannot be showen in mobile as per the design. Normally, navigation switches to hamburger menu which was out of scope for this project. The responsive behaviour is limited to tablet and desktop only. For mobile the layout seems to be broken a little. 
 Note: Only header navigation will not be as per the design in mobile. Rest would still look normal.
4. For alternative ways for charts, you can uncomment HTML code in login.component.html. The result would be shown in other tabs that is not in Verbal CR, but in rest on the tabs.
 Note: This data is just the showcase of alternative ways, not much details have been given to this. -->

<!-- How to open up the project:- -->
<!-- 
Prerequisite:-
1. Node, npm is required. Install angular.
2. go to project structure and open command line. Do npm install and let dependencies download.
3. do ng serve or npm start (do ng serve internally).
4. open up the project. (mostly localhost:4200) -->